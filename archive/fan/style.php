<?php
    header("Content-type: text/css; charset: UTF-8");

$hexvalues = array('#fef769','#af6ec8','#fb5555','#f0caa5','#be745b','#f9c2c7','#d7cbbd', '#d5bfb4', '#ead7b6');

?>

@font-face {
    font-family: 'bellcent_subcap_btsub-caption';
    src: url('bellcent.eot');
    src: url('bellcent.eot?#iefix') format('embedded-opentype'),
         url('bellcent.woff2') format('woff2'),
         url('bellcent.woff') format('woff'),
         url('bellcent.ttf') format('truetype'),
         url('bellcent.svg#bellcent_subcap_btsub-caption') format('svg');
    font-weight: normal;
    font-style: normal;

}

* {
	font-family: BellCentennial-SubCaption, sans-serif;	
	background-color: white;
}

*:focus {
    outline: 0;
}

a {
	color:black;
	text-decoration: none;
}

#interface {
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	margin: auto;
	align-items: center;
	display: flex;
  	justify-content: center;

}

input[type=text]{
	font-family: BellCentennial-SubCaption, sans-serif;
	font-size: 3em;
	opacity: 0.7;
	cursor: normal;
	text-indent: 10px;
}

input[type=text]:hover{
	opacity: 1;
}

input {	
	border-style: solid;
	border-color: red;
	border-top: 0;
	border-left: 0;
	border-right: 0;
	text-align: left;
	opacity: 0.7;

}

#info_btn {
	position: fixed;
	bottom: 5%;
	right: 5%;
	z-index: 999;
}

#showinfo {
	z-index: 1001;
	position: fixed;
	background-color: red;
	width: 500px;
	height: 500px;
	left: 50%;
	top: 50%;
	
}



#mailall {
width: 200px;
height: 60px;
	color: black;
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	margin: auto;
	align-items: center;
	display: flex;
  	justify-content: center;
	font-size: 40px;
	-webkit-box-shadow: 1px 1px 158px 49px rgba(255,255,255,1);
	-moz-box-shadow: 1px 1px 158px 49px rgba(255,255,255,1);
	box-shadow: 1px 1px 158px 49px rgba(255,255,255,1);
}

#mailall:hover {
	color: red;
}

#send {
	font-family: BellCentennial-SubCaption, sans-serif;
	font-size: 3em;
	margin-top: 10px;
	color: red;
	text-align: center;
	align: center;
	display:block;
}

#send:hover {
opacity: 1;
}