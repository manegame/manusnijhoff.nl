var ver, hor;

var addedClass;

var mouseDown = 0;

document.onmousedown = function() { 
  ++mouseDown;
}
document.onmouseup = function() {
  --mouseDown;
}

var drop = document.getElementsByClassName('dropDiv');

function make() {

	var bod = document.getElementsByTagName('body')[0];

	for (var i = 1 ; i <= hor; i ++ ) {
		for (var j = 1;  j <= ver ; j ++ ) {
			var div = document.createElement('div');
			div.setAttribute('class', 'dropDiv '+ addedClass +'');

			bod.appendChild(div);
		}
	}

	$("html, body").animate({ scrollTop: $(document).height() }, 1000);

//add event listeners here

	for ( var i = 0 ; i < drop.length ; i ++ ) {				

    		drop[i].addEventListener('click', function(){
				if(this.classList.contains('dropDiv')) {
					var img = document.createElement('img');
    				img.setAttribute('class', 'dragImg');
					var randsrc = Math.floor(Math.random() * 86) + 1;
					img.src = "../img/" + randsrc + ".jpg";
					this.classList.remove('dropDiv');
					this.classList.add('imgDiv', 'dropHere');
					this.appendChild(img);
				}
			});

    		drop[i].addEventListener('mouseover', function(){
 				if( mouseDown == 1 ) {
        			if(this.classList.contains('dropDiv')) {
        				var img = document.createElement('img');
						img.setAttribute('class', 'dragImg');
        				var randsrc = Math.floor(Math.random() * 86) + 1;
        				img.src = "../img/" + randsrc + ".jpg";
        				this.classList.remove('dropDiv');
	        			this.classList.add('imgDiv', 'dropHere');
						this.appendChild(img);
        			}
				} else {
			    }
			});
		}
	

}

function three() {

	addedClass = "three";
	hor = 3;
	ver = 3;

	make();

}

function nine() {

	addedClass = "nine";

	hor = 9;
	ver = 9;

	make();
	
}

function twentySeven() {

	addedClass = "twenty-seven";

	hor = 27;
	ver = 27;
	

	make();
}

function fiftyFour() {

	addedClass = "fifty-four";

	hor = 54;
	ver = 54;

	make();
	
}

function redBg() {
	$('body').addClass('redBg');
	$('body').removeClass('greenBg blueBg blackBg');
	$('.buttonbar').css("color", "black");
	$('.triggerbuttons').css("color", "black");
}

function greenBg() {
	$('body').addClass('greenBg');
	$('body').removeClass('redBg blueBg blackBg');
	$('.buttonbar').css("color", "black");
	$('.triggerbuttons').css("color", "black");
}

function blueBg() {
	$('body').addClass('blueBg');
	$('body').removeClass('redBg greenBg blackBg');
	$('.buttonbar').css("color", "white");
	$('.triggerbuttons').css("color", "white");
}

function blackBg() {
	$('body').addClass('blackBg');
	$('body').removeClass('redBg greenBg blueBg');	
	$('.buttonbar').css("color", "white");
	$('.triggerbuttons').css("color", "white");
}

function whiteBg() {
	$('body').removeClass('redBg greenBg blueBg blackBg');	
	$('.buttonbar').css("color", "black");
	$('.triggerbuttons').css("color", "black");
}

$(document).ready(function(){

	$('.triggerbuttons').on('click', function(){

		$('.welcome').fadeOut('slow');
		$('.buttonbar').fadeToggle('fast');
		$('.triggerbuttons').toggleClass('active');
		
		if( $('.buttonbar').not(":hover") ){

		console.log('nu weg');
		window.setTimeout(function(){
				console.log('weg');
						if( $('.buttonbar').is(':visible') ){
							window.setTimeout(function(){
						$('.buttonbar').fadeOut('fast');
					}, 5000);
				}
					
				$('.triggerbuttons').removeClass('active');
			}, 13000);
		}
	});

	$('.enjoy').on("click" , function() {

		var timing = document.body.scrollHeight;
		console.log(timing);

	    $('html, body').animate({ scrollTop: 0 }, timing, 'linear', function () {
	    });
	});

	function greyScale() {
		$('img').toggleClass('desaturate');
	}

	function invert() {
		$('img').toggleClass('invert');
	}

	$('.greyscaler').on('click', function() {
		greyScale();
	});

	$('.inverter').on('click', function() {
		invert();
	});

	$(document).disableSelection();

// DROP

	function handleDropEvent( event, ui ) {
 		var draggable = ui.draggable;
 		console.log('ja hoor');
	}

	$(window).bind('keypress', function(e) {

		// alert('read');
		var code = e.keyCode || e.which;

		console.log(code);

		if( code == 98 || code == 66) {
			greyScale();
		} else if ( code == 105 || code == 73 ){
			invert();
		}
	});
		


});


