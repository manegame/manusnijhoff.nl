
  $(document).ready(function(){ 

    $(document)
    .on('touchstart', function(){
        $("#title").toggleClass("hover_effect");
        $(this).toggleClass("hover_effect");
    })
    .on('touchend', function(){
        $("#title").toggleClass("hover_effect");
        $(this).toggleClass("hover_effect");
    })
    ;

    // $('.hover').bind('touchstart touchend', function(e) {
    //     console.log('nu');
    //     e.preventDefault();
    //     $(this).toggleClass('hover_effect');
    //     });

    // $('.site').hide();
    
    var scroll_pos = 0;
    var animation_0_pos = 0; //where you want the animation to begin
    var animation_1_pos = ($(document).height() / 4); //where you want the animation to stop
    var animation_2_pos = (($(document).height() / 4) * 2);
    var animation_3_pos = $(document).height(); 

    var first_color = new $.Color( 'rgb(0, 0, 77)' ); //we can set this here, but it'd probably be better to get it from the CSS; for the example we're setting it here.
    var second_color = new $.Color('rgb(0,0,220)');
    var third_color = new $.Color('rgb(0,0,77)');
    var fourth_color = new $.Color( 'rgb(0, 0, 220)' ); ;//what color we want to use in the end

    $(document).scroll(function() {
        
        scroll_pos = $(this).scrollTop(); 

        // console.log(scroll_pos);	

        if(scroll_pos >= animation_0_pos && scroll_pos <= animation_1_pos ){

        	//we want to calculate the relevant transitional rgb value
            var percentScrolled = scroll_pos / ( animation_1_pos - animation_0_pos );
            var newRed = first_color.red() + ( ( second_color.red() - first_color.red() ) * percentScrolled );
            var newGreen = first_color.green() + ( ( second_color.green() - first_color.green() ) * percentScrolled );
            var newBlue = first_color.blue() + ( ( second_color.blue() - first_color.blue() ) * percentScrolled );
            var newColor = new $.Color( newRed, newGreen, newBlue );

            //console.log( newColor.red(), newColor.green(), newColor.blue() );
            $('body').animate({ backgroundColor: newColor }, 0);

        } else if ( scroll_pos >= animation_1_pos && scroll_pos < animation_2_pos ) {


        	//we want to calculate the relevant transitional rgb value
            var percentScrolled = scroll_pos / ( animation_2_pos - animation_1_pos );
            //console.log(percentScrolled + '%' +  (third_color.blue() - second_color.blue()) * percentScrolled);
            var newRed = second_color.red() + ( ( third_color.red() - second_color.red() ) * (percentScrolled - 1) );
            var newGreen = second_color.green() + ( ( third_color.green() - second_color.green() ) * (percentScrolled - 1) );
            var newBlue = second_color.blue() + ( ( third_color.blue() - second_color.blue() ) * (percentScrolled - 1) );
            var newColor = new $.Color( newRed, newGreen, newBlue );

            $('body').animate({ backgroundColor: newColor }, 0);

            // console.log( newColor.red(), newColor.green(), newColor.blue() );

        } else if( scroll_pos >= animation_2_pos && scroll_pos <= animation_3_pos ) {

        	//we want to calculate the relevant transitional rgb value
            var percentScrolled = scroll_pos / ( animation_3_pos - animation_2_pos );
            console.log(percentScrolled);
            var newRed = third_color.red() + ( ( fourth_color.red() - third_color.red() ) * ((percentScrolled - 1) * 22 ) );
            var newGreen = third_color.green() + ( ( fourth_color.green() - third_color.green() ) * ((percentScrolled - 1) * 2 )  );
            var newBlue = third_color.blue() + ( ( fourth_color.blue() - third_color.blue() ) * ((percentScrolled - 1) * 2 )  );
            var newColor = new $.Color( newRed, newGreen, newBlue );

            $('body').animate({ backgroundColor: newColor }, 0);

            // console.log( newColor.red(), newColor.green(), newColor.blue() );
        } else {

        }
             
    });


    // $('ul.artist li').hover(function(){
    //     var site = $(this).find('a');
    //     var text = $(this).find('h1');
    //     text.animate({color:'white'}, 200);
    //     site.fadeIn(200);
    // }, 
    //     function(){
    //          var site = $(this).find('a');
    //          var text = $(this).find('h1');
    //         text.animate({color:'black'}, 200);
    //         site.fadeOut(200);
    //    // $('ul.artist li .site').fadeOut(200); 
    // }
    // );
        

});
  

