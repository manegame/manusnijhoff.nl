$( document ).ready( function()
{
	var mousePointerSize;
	$( '<img/>' ).attr( 'src', $( 'img' ).attr( 'src' ) ).load( function()
	{
		mousePointerSize = { 'width' : this.width, 'height' : this.height };
	});

	window.onmousemove = function( e )
	{
		var mouseX = e.clientX;
		var mouseY = e.clientY;
		$( '#mousePointer' ).css( { 'left' : mouseX, 'top' : mouseY } );
	}
});