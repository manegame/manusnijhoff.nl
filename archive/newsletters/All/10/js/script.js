$( document ).ready( function()
{
	var mousePointerSize;
	$( '<img/>' ).attr( 'src', $( 'img' ).attr( 'src' ) ).load( function()
	{
		mousePointerSize = { 'width' : this.width , 'height' : this.height };
	});

	window.onmousemove = function( e )
	{
		var mouseX = e.pageX;
		var mouseY = e.pageY;

		var curSrc = $(' #mousePointer ').attr('src');
		if( curSrc == 'cursor2.png' ) {
			console.log(curSrc);
		} else {
			$( '#mousePointer' ).css( { 'left' : mouseX - 23, 'top' : mouseY - 89 } );	
		}
		
	}

	window.onmousedown = function() {
		$(' #mousePointer ').attr('src', 'cursor2.png');	
	} 

	window.onmouseup = function() {
		$(' #mousePointer ').attr('src', 'cursor.png');	
	}
});