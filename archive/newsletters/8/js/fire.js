/**
 *  @name  Delay
 *  @description
 *  Click the mouse to hear the p5.Delay process a SoundFile.
 *  MouseX controls the p5.Delay Filter Frequency.
 *  MouseY controls both the p5.Delay Time and Resonance.
 *  Visualize the resulting sound's volume with an Amplitude object.
 *
 * <p><em><span class="small"> To run this example locally, you will need the
 * <a href="http://p5js.org/reference/#/libraries/p5.sound">p5.sound library</a>
 * a sound file, and a running <a href="https://github.com/processing/p5.js/wiki/Local-server">local server</a>.</span></em></p>
 */

var sound, reverb, delay;

function preload() {
  soundFormats('mp3');
  soundFile = loadSound('http://magical.ml/newsletters/8/js/assets/campfire')
  soundFile.disconnect();
  
}

function setup() {
  
  createCanvas(1, 1);

  delay = new p5.Delay();
  delay.process(soundFile, .1, .4, 700);
  delay.setType('pingPong'); // a stereo effect

  // reverb = new p5.Reverb();
  // reverb.process(soundFile, 8, 10);

  soundFile.play();
  // soundFile.loop();
}

function draw() {
  
}