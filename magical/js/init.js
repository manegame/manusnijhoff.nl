// This is a manegame production

// 👀
// 👃
// 👅


	$(document).ready(function(){

		//Loding images

		$('img').addClass('lazy');

		$(function() {
        	$('.lazy').Lazy();
    	});

		//show button on scroll

		userHasScrolled = false;

		$(document).scroll(function(){
			if(userHasScrolled == false){
				userHasScrolled = true;

				if(userHasScrolled){

				$('#switch').fadeIn(200);
					setTimeout(function(){
						$('#switch').fadeOut(200);
						userHasScrolled = false;
					}, 4000);
				}	
			}
			
		});

		var bod = $('body, a, a:active, a:visited, a:hover, #aboutDiv, #switch'); 
		var a = $('a, a:active, a:visited, a:hover');
		var switcher = $('#switch');

		//Determine random color on load

		var random = Math.random();

		if( random < 0.3333333 ){
			bod.addClass('black');
			switcher.addClass('blackA');
		} else if( random > 0.6666666 ){
			bod.addClass('white');
			switcher.addClass('whiteA');
		} else {
			bod.addClass('yellow');
			switcher.addClass('yellowA');
		}

		//Switch colors on button click

		$('#switch').click(function(){
			if(bod.hasClass('black')){
				bod.removeClass('black').addClass('white');
				switcher.removeClass('blackA').addClass('whiteA');
			} else if(bod.hasClass('white')) {
				bod.removeClass('white').addClass('yellow');
				switcher.removeClass('whiteA'// This is a manegame production

// 👀
// 👃
// 👅
).addClass('yellowA');
			} else {
				bod.removeClass('yellow').addClass('black');
				switcher.removeClass('yellowA').addClass('blackA');
			}
		}); 

		//change image appearance on click

		$('img').on('click', function(){
			var img = $(this);
			var container = $(this).parent();

			if(img.hasClass('img-full')){
				container.addClass('padded');
				img.removeClass('img-full');
			} else {
				container.removeClass('padded');
				img.addClass('img-full');		
			}
			
		});

			$('#title, #aboutDiv').click(function(){
			$('#aboutDiv').toggle();
		});

	});